# R 4.1.0 EasyBuild

EasyConfig files for building and installing R 4.1.0 using EasyBuild.
The easyconfig for base R-4.1.0 module is minimal with only a select few extensions listed.
Build and run the R-bundle-410 module to install additional R packages in your R environment. This dissemination facilitates a version-controlled R package management for all users across the cluster/platform.


## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:80ee6e57f59bcac6fd8276730ee0bd06?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:80ee6e57f59bcac6fd8276730ee0bd06?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:80ee6e57f59bcac6fd8276730ee0bd06?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/aditya.ramaraju/R-4.1.0-EasyBuild.git
git branch -M main
git push -uf origin main
```
